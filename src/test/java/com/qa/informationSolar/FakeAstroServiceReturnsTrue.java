package com.qa.informationSolar;

public class FakeAstroServiceReturnsTrue implements IAstroService{

    boolean isAuthenticated = true;

    @Override
    public boolean authenticate(String userID, String password) {
        isAuthenticated = true;
        getStatusInfo();
        return true;
    }

    @Override
    public String getStatusInfo() {
        return ("AOC, ObjectType, ObjectName, OrbitalPeriod, Radius, SemiMajorAxis, Mass");

    }
}
