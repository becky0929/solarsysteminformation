package com.qa.informationSolar;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;


public class SolarSystemInformationTest {


    IAstroService interfaceOption = new FakeAstroServiceReturnsTrue();
    IAstroService astroService;

    private String validUserID = "FD4444";
    private String validPassword = "12345678Ab!";

    @BeforeEach
    void setUp() {
        astroService = createMock(IAstroService.class);
    }

    @Test
    void authentication_is_called () throws FormattingException {
        expect(astroService.authenticate(validUserID, validPassword)).andReturn(true);
        EasyMock.expectLastCall().once();
        replay(astroService);

        SolarSystemInformation cut = new SolarSystemInformation(validUserID,validPassword,astroService);
        verify(astroService);


    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
//    @Test
//    public void test_of_mock(){
//        EasyMock.expect()
//    }

    //////////////////////////////////////////////////////////////////////////////////////////////USERNAME TESTS
    @Test
    public void validation_for_correct_amount_of_characters_in_username() throws FormattingException {
        String userNameIn = "NH3456";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = null;
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void validation_test_if_first_two_items_are_chars_and_last_four_are_ints() throws FormattingException {
        String userNameIn = "ED4567";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = null;
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_test_if_four_zeros_in_userID() throws FormattingException {
        String userNameIn = "ED0000";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_test_for_symbols_instead_of_chars_and_ints() throws FormattingException {
        String userNameIn = "£^$()@";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_test_if_all_spaces_are_numbers() throws FormattingException {
        String userNameIn = "123456";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_test_if_all_spaces_are_characters() throws FormattingException {
        String userNameIn = "ABCDEF";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_test_if_all_characters_are_lower_case() throws FormattingException {
        String userNameIn = "ab1234";
        String passwordIn = "Abcdefhi1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////PASSWORD TESTS

    @Test
    public void validation_check_for_at_least_ten_characters_in_password() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        String expectedReturn = null;
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_check_when_less_than_10_characters() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "1234567";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void validation_check_for_capital_lowercase_symbols_numbers() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        String expectedReturn = null;
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_for_password_with_no_capitals() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "abcdefghi1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_for_password_with_no_lowercase() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "ABCDEFGHI1!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_for_password_with_no_numbers() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "abcdefghik!";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    @Test
    public void invalid_for_password_with_no_symbols() throws FormattingException {
        String userNameIn = "AB1234";
        String passwordIn = "abcdefghi1h";
        String expectedReturn = "Not Allowed";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualObjectNameReturn = cut.getObjectName();
        String actualObjectTypeReturn = cut.getObjectType();

        assertEquals(expectedReturn, actualObjectNameReturn);
        assertEquals(expectedReturn, actualObjectTypeReturn);
    }

    /////////////////////////////////////////////////////////////////////////////////////AOC TESTS

    @Test
    public void valid_test_for_format_of_AOC() throws FormattingException {
        String codeIn = "SSun27TL";
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        String expectedReturn = "VALID";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        String actualReturn = cut.initialiseAOCDetails(codeIn, interfaceOption);

        assertEquals(expectedReturn, actualReturn);
    }



    @Test
     public void when_it_throws_an_exception() throws FormattingException {
        String codeIn = "Sun27TL";
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);

        assertThrows(FormattingException.class,
                () -> cut.initialiseAOCDetails(codeIn, interfaceOption));
    }

    /////////////////////////////////////FAKE ASTRO AUTHENTICATE/////////////////////////////////////////////////////////////////

    @Test
    public void authenticate_method_returns_true_in_fake_webservice(){
        boolean expectedReturn = true;
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        FakeAstroServiceReturnsTrue cut = new FakeAstroServiceReturnsTrue();

        boolean actualReturn = cut.authenticate(userNameIn,passwordIn);

        assertEquals(expectedReturn, actualReturn);
    }

    @Test
    public void authenticate_method_returns_false_in_fake_webservice(){
        boolean expectedReturn = false;
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        FakeAstroServiceReturnsFalse cut = new FakeAstroServiceReturnsFalse();

        boolean actualReturn = cut.authenticate(userNameIn,passwordIn);

        assertEquals(expectedReturn, actualReturn);
    }

    /////////////////////////////FAKE ASTRO GET INFO//////////////////////////////////////////////////////

    @Test
    public void check_return_when_userId_and_password_is_authorised(){
        String expectedReturn ="AOC, ObjectType, ObjectName, OrbitalPeriod, Radius, SemiMajorAxis, Mass";
        FakeAstroServiceReturnsTrue cut = new FakeAstroServiceReturnsTrue();

        String actualReturn = cut.getStatusInfo();

        assertEquals(expectedReturn,actualReturn);

    }
    @Test
    public void check_return_when_userId_and_password_is_not_authorised(){
        String expectedReturn = "No such astronomical object classification code";
        FakeAstroServiceReturnsFalse cut = new FakeAstroServiceReturnsFalse();

        String actualReturn = cut.getStatusInfo();

        assertEquals(expectedReturn,actualReturn);

    }

    /////////////////////////////////AOC RETURN TESTS/////////////////////////////////////////////////

    @Test
    public void check_aoc_has_been_returned() throws FormattingException {
        String expectedReturn = "AOC, ObjectType, ObjectName, OrbitalPeriod, Radius, SemiMajorAxis, Mass(KG)";
        String userNameIn = "AB1234";
        String passwordIn = "Abcdefghi1!";
        SolarSystemInformation cut = new SolarSystemInformation(userNameIn, passwordIn, interfaceOption);
        cut.initialiseAOCDetails("SSun27TL", interfaceOption);

        String actualReturn = cut.getAstronomicalObjectClassificationCode();

        assertEquals(expectedReturn,actualReturn);
    }

}

