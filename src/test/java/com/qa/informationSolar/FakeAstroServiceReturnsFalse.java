package com.qa.informationSolar;

public class FakeAstroServiceReturnsFalse implements IAstroService{

    boolean isAuthenticated = false;

    @Override
    public boolean authenticate(String userID, String password) {
        isAuthenticated = false;
        getStatusInfo();
        return false;
    }

    @Override
    public String getStatusInfo() {
        return("No such astronomical object classification code");
    }
}
