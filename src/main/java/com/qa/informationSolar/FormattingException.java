package com.qa.informationSolar;

public class FormattingException extends Exception {
    private String message;

    public FormattingException(String errorMessage) {
        message = errorMessage;
    }

    public String getMessage(){
        return message;
    }
}
