package com.qa.informationSolar;

public interface IAstroService {

    boolean authenticate(String userID, String password);

    String getStatusInfo();
}
