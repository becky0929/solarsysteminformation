package com.qa.informationSolar;

import java.math.BigDecimal;
import java.math.BigInteger;

public class SolarSystemInformation {

    private String userID;
    private String password;
    private String astronomicalObjectClassificationCode;
    private String objectType;
    private String objectName;
    private boolean exists;
    private BigInteger orbitalPeriod;
    private BigDecimal radius;
    private BigDecimal semiMajorAxis;
    private BigDecimal mass;


    public SolarSystemInformation(String userID, String password, IAstroService as) throws FormattingException {
        this.userID = userID;
        this.password = password;
       // boolean validationString = false;



        if(userID.matches("[A-Z]{2}[0-9]{4}") && !userID.endsWith("0000") &&
                password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,}$")){

            as.authenticate(userID, password);
            //validationString  = true;
            //initialiseAOCDetails(astronomicalObjectClassificationCode, as);
        }
        else{
            setObjectName("Not Allowed");
            setObjectType("Not Allowed");
            throw new FormattingException("wrong");
        }
    }

    public String initialiseAOCDetails(String AOC, IAstroService as) throws FormattingException{
        String validConfirm = "INVALID";
        if (AOC.matches("^[S,P,M,D,A,C][1-9]{0,8}[A-Z][a-z]{2}[1-9]{1,3}[T,M,B,L]{1,2}")) {
            validConfirm = "VALID";
        }
        else{
            throw new FormattingException("This is not formatted correctly");
        }
        setAstronomicalObjectClassificationCode(as.getStatusInfo());
        return validConfirm;
    }

    public String getAstronomicalObjectClassificationCode() {
        return astronomicalObjectClassificationCode;
    }

    private void setAstronomicalObjectClassificationCode(String astronomicalObjectClassificationCode) {
        this.astronomicalObjectClassificationCode = astronomicalObjectClassificationCode;
    }

    public String getObjectType() {
        return objectType;
    }

    private void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectName() {
        return objectName;
    }

    private void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public boolean isExists() {
        return exists;
    }

    private void setExists(boolean exists) {
        this.exists = exists;
    }

    public BigInteger getOrbitalPeriod() {
        return orbitalPeriod;
    }

    private void setOrbitalPeriod(BigInteger orbitalPeriod) {
        this.orbitalPeriod = orbitalPeriod;
    }

    public BigDecimal getRadius() {
        return radius;
    }

    private void setRadius(BigDecimal radius) {
        this.radius = radius;
    }

    public BigDecimal getSemiMajorAxis() {
        return semiMajorAxis;
    }

    private void setSemiMajorAxis(BigDecimal semiMajorAxis) {
        this.semiMajorAxis = semiMajorAxis;
    }

    public BigDecimal getMass() {
        return mass;
    }

    private void setMass(BigDecimal mass) {
        this.mass = mass;
    }
}
